/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 19:49:54 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/03 18:13:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_H
# define TESTS_H

/*
** INCLUDES:
** stdbool.h: bool type
*/

#include <stdbool.h>

bool	test_strlen(void);
bool	test_strcpy(void);
bool	test_strcmp(void);
bool	test_write(void);
bool	test_read(void);
bool	test_strdup(void);

#endif
