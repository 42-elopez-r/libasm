extern _ft_strlen
extern _malloc
extern _ft_strcpy

section .text
	global _ft_strdup
_ft_strdup:
	push rsi  ; Save copy of whatever is in rsi to the stack (to be restored at the end)
	push r12
	push r13  ; To align the stack to 16 bytes so malloc is happy
	mov r12, rdi  ; Save s1 string in r12
	call _ft_strlen
	mov rdi, rax ; Move the length of s1 to the first parameter, for malloc
	inc rdi  ; For the terminating null byte
	call _malloc
	cmp rax, 0  ; Check if malloc failed
	je __malloc_error  ; Carry flag tells an error happened
	mov rdi, rax  ; Save the allocated pointer to rdi (for the ft_strcpy call)
	mov rsi, r12
	call _ft_strcpy
	mov rax, rdi
	pop r13
	pop r12
	pop rsi
	ret
__malloc_error:
	mov rax, 0
	pop r13
	pop r12
	pop rsi
	ret
