section .text
	global _ft_strcpy
_ft_strcpy:
	; Save previous values of registers used to the stack
	push rcx
	push r12
	mov rcx, 0  ; Index of current byte of src string
__loop_strcpy:
	cmp byte [rsi + rcx], 0  ; Check if current byte of src string is '\0'
	je __end_loop_strcpy
	mov r12b, byte [rsi + rcx]  ; Copy current byte of src to intermediate register
	mov byte [rdi + rcx], r12b  ; Copy intermediate register to current byte of dst string
	inc rcx
	jmp __loop_strcpy
__end_loop_strcpy:
	mov byte [rdi + rcx], 0  ; Terminating '\0'
	mov rax, rdi
	; Restore registers saved in the stack
	pop r12
	pop rcx
	ret
