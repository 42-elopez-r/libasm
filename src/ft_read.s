extern ___error

SYS_READ EQU 0x02000003

section .text
	global _ft_read
_ft_read:
	push r12
	mov rax, SYS_READ
	syscall
	jnc __end  ; Carry flag tells an error happened
	mov r12, rax  ; Save write() return to r12
	call ___error
	mov [rax], r12
	mov rax, -1
__end:
	pop r12
	ret
