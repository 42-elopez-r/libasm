section .text
	global _ft_strlen
_ft_strlen:
	mov rax, 0
__loop_strlen:
	cmp byte [rdi + rax], 0  ; Check if current byte is '\0'
	je __end_loop_strlen
	inc rax
	jmp __loop_strlen
__end_loop_strlen:
	ret
