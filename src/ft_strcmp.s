section .text
	global _ft_strcmp
_ft_strcmp:
	push r12
	push r13
	push r14
	mov rax, 0
__loop_strcmp:
	; Check if s1[rax] and s2[rax] are both '\0'
	cmp byte [rdi + rax], 0
	jne __end_test_equals
	cmp byte [rsi + rax], 0
	je __s1_s2_equals
__end_test_equals:
	; Check if the characters are different
	mov r12b, byte [rsi + rax]  ; Copy s2[rax] to auxiliar r12b
	cmp byte [rdi + rax], r12b
	jne __diff
	inc rax
	jmp __loop_strcmp
__diff:
	mov r13, rax  ; Save the index to r13 so return can be stored in rax
	mov rax, 0  ; Reset rax
	mov al, byte [rdi + r13]
	mov r14, 0
	mov r14b, byte [rsi + r13]
	sub eax, r14d  ; Use 4 bytes registers so sign ends in the right bit
	jmp __strcmp_end
__s1_s2_equals:
	mov rax, 0
__strcmp_end:
	pop r14
	pop r13
	pop r12
	ret
