extern ___error

SYS_WRITE EQU 0x02000004

section .text
	global _ft_write
_ft_write:
	push r12
	mov rax, SYS_WRITE
	syscall
	jnc __end  ; Carry flag tells an error happened
	mov r12, rax  ; Save write() return to r12
	call ___error
	mov [rax], r12
	mov rax, -1
__end:
	pop r12
	ret
