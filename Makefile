BIN_FORMAT = macho64

CC = cc
NASM = nasm
AR = ar
SHELL = /bin/sh
CFLAGS += -Iinclude -Wall -Werror -Wextra

NAME = libasm.a
NAME_TESTS = tests

SRCS_LIBASM = $(shell find src -name "*.s")
OBJS_LIBASM = $(SRCS_LIBASM:.s=.o)

SRCS_TESTS = $(shell find src_tests -name "*.c")
OBJS_TESTS = $(SRCS_TESTS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS_LIBASM)
	$(AR) -rcs $(NAME) $(OBJS_LIBASM)

$(NAME_TESTS): $(NAME) $(OBJS_TESTS)
	$(CC) $(CFLAGS) -o $(NAME_TESTS) $(OBJS_TESTS) $(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

.s.o:
	$(NASM) -f $(BIN_FORMAT) $<

clean:
	find . -name "*.o" -delete -print

fclean: clean
	rm -f $(NAME) $(NAME_TESTS)

re: fclean all

debug: CFLAGS += -g
debug: fclean tests

.PHONY: all clean fclean re debug
