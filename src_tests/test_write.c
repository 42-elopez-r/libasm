/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_write.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:08:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/01 19:55:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_write()
** stdio.h: printf(), puts(), remove()
** fnctl.h: open()
** string.h: strlen(), strcmp()
** unistd.h: close(), read()
** stdlib.h: free(), calloc(), realloc()
** errno.h: errno
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define TMP_FILE1 "dont_mind_me_just_a_temp_file"
#define TMP_FILE2 "dont_mind_me_just_another_temp_file"
#define ERR_TMP_FILE1 "/dont_mind_me_just_a_temp_file"
#define ERR_TMP_FILE2 "/dont_mind_me_just_another_temp_file"

/*
** This function reads a file returning a string with its content or NULL
** in case of error.
*/

static char *
read_file(const char *filename)
{
	int fd;
	char *read_str;
	size_t i;
	char buf;

	if ((fd = open(filename, O_RDONLY)) < 0)
		return (NULL);
	read_str = calloc(1, 1);
	i = 0;
	while (read(fd, &buf, 1) == 1)
	{
		read_str = realloc(read_str, i + 2);
		read_str[i] = buf;
		i++;
	}
	close(fd);
	return (read_str);
}

/*
** This function runs write and its libasm equivalent comparing its results.
** Returns true if they match.
*/

static bool	case_write(const char *str, bool successful)
{
	ssize_t ret1, ret2;
	int errno1, errno2;
	int fd;
	char *read_str_orig, *read_str_asm;

	/* ASM ft_write() */
	fd = open(successful ? TMP_FILE1 : ERR_TMP_FILE1,
			O_WRONLY | O_CREAT | O_TRUNC, 0666);
	errno = 0;
	ret1 = ft_write(fd, str, strlen(str));
	errno1 = errno;
	if (fd != -1)
		close(fd);

	/* libc write() */
	fd = open(successful ? TMP_FILE2 : ERR_TMP_FILE2,
			O_WRONLY | O_CREAT | O_TRUNC, 0666);
	errno = 0;
	ret2 = write(fd, str, strlen(str));
	errno2 = errno;
	if (fd != -1)
		close(fd);

	read_str_asm = read_file(successful ? TMP_FILE1 : ERR_TMP_FILE1);
	remove(successful ? TMP_FILE1 : ERR_TMP_FILE1);
	read_str_orig = read_file(successful ? TMP_FILE2 : ERR_TMP_FILE2);
	remove(successful ? TMP_FILE2 : ERR_TMP_FILE2);
	printf("write(): \"%s\" ft_write(): \"%s\"\n", read_str_orig, read_str_asm);
	printf("Orig: %zd ASM: %zd\n", ret2, ret1);
	printf("Errno orig: %d Errno ASM: %d\n", errno2, errno1);
	if (((bool)read_str_asm != (bool)read_str_orig) ||  /* XOR */
			(read_str_asm && read_str_orig &&
			strcmp(read_str_asm, read_str_orig) != 0))
	{
		free(read_str_asm);
		free(read_str_orig);
		return (false);
	}
	free(read_str_asm);
	free(read_str_orig);
	return (ret1 == ret2 && errno1 == errno2);
}

/*
** Test for the write function. Returns true if all cases succeed.
*/

bool	test_write(void)
{
	puts("=== write() tests ===");
	puts("=> Successfull cases");
	if (!case_write("hello world", true))
		return (false);
	if (!case_write("Luna > Celestia", true))
		return (false);
	if (!case_write("I use Arch BTW", true))
		return (false);
	if (!case_write("", true))
		return (false);
	if (!case_write(" ", true))
		return (false);
	puts("=> Unsuccessfull case");
	if (!case_write("hello world", false))
		return (false);
	return (true);
}
