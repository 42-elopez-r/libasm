/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strcpy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:29:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/01 15:42:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_strcpy()
** stdio.h: printf(), puts(), fprintf(), fputs()
** string.h: strlen(), strcmp()
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_MAX 200

/*
** This function runs ft_strcpy and checks that its return value is the same as
** the destination buffer and that the copied string is the same as str.
** Returns true if the case passes.
*/

static bool	case_strcpy(const char *str)
{
	char	*ret;
	char	buffer[BUFFER_MAX];

	if (strlen(str) >= BUFFER_MAX)
	{
		fprintf(stderr, "The string is too long to be tested: \"%s\"\n",
				str);
		fputs("Increment BUFFER_MAX and recompile\n", stderr);
		return (false);
	}
	ret = ft_strcpy(buffer, str);
	printf("str: \"%s\" Copied: \"%s\"\n", str, buffer);
	printf("Orig: %p ASM: %p\n", buffer, ret);
	return (ret == buffer && strcmp(str, buffer) == 0);
}

/*
** Test for the strcpy function. Returns true if all cases succeed.
*/

bool	test_strcpy(void)
{
	puts("=== strcpy() tests ===");
	if (!case_strcpy("hello world"))
		return (false);
	if (!case_strcpy("Luna > Celestia"))
		return (false);
	if (!case_strcpy("I use Arch BTW"))
		return (false);
	if (!case_strcpy(""))
		return (false);
	if (!case_strcpy(" "))
		return (false);
	return (true);
}
