/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:08:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/27 21:35:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_strlen()
** stdio.h: printf(), puts()
** string.h: strlen()
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <string.h>

/*
** This function runs strlen and its libasm equivalent comparing its results.
** Returns true if they match.
*/

static bool	case_strlen(const char *str)
{
	size_t	ret1;
	size_t	ret2;

	ret1 = strlen(str);
	ret2 = ft_strlen(str);
	printf("str: \"%s\"\n", str);
	printf("Orig: %zu ASM: %zu\n", ret1, ret2);
	return (ret1 == ret2);
}

/*
** Test for the strlen function. Returns true if all cases succeed.
*/

bool	test_strlen(void)
{
	puts("=== strlen() tests ===");
	if (!case_strlen("hello world"))
		return (false);
	if (!case_strlen("Luna > Celestia"))
		return (false);
	if (!case_strlen("I use Arch BTW"))
		return (false);
	if (!case_strlen(""))
		return (false);
	if (!case_strlen(" "))
		return (false);
	return (true);
}
