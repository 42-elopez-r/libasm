/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strcmp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:10:17 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/27 20:26:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_strcmp()
** stdio.h: printf(), puts()
** string.h: strcmp()
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <string.h>

/*
** This function runs strcmp and its libasm equivalent comparing its results.
** Returns true if they match.
*/

static bool	case_strcmp(const char *s1, const char *s2)
{
	int	ret1;
	int	ret2;

	ret1 = strcmp(s1, s2);
	ret2 = ft_strcmp(s1, s2);
	printf("s1: \"%s\" s2: \"%s\"\n", s1, s2);
	printf("Orig: %d ASM: %d\n", ret1, ret2);
	return (ret1 == ret2);
}

/*
** Test for the strcmp function. Returns true if all cases succeed.
*/

bool	test_strcmp(void)
{
	puts("=== strcmp() tests ===");
	if (!case_strcmp("hola", "hola"))
		return (false);
	if (!case_strcmp("hola", "hol"))
		return (false);
	if (!case_strcmp("hol", "hola"))
		return (false);
	if (!case_strcmp("hoba", "hola"))
		return (false);
	if (!case_strcmp("hola", "hoba"))
		return (false);
	return (true);
}
