/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:48:59 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/03 19:43:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: test_strlen(), test_strcpy(), test_strcmp()
** stdio.h: puts()
*/

#include <tests.h>
#include <stdio.h>

int main(void)
{
	puts("=================================");
	puts("elopez-r's Libasm Test Collection");
	puts("=================================\n");
	if (!test_strlen())
		return (1);
	if (!test_strcpy())
		return (1);
	if (!test_strcmp())
		return (1);
	if (!test_write())
		return (1);
	if (!test_read())
		return (1);
	if (!test_strdup())
		return (1);
	puts("\n==============================================");
	puts("All tests passed SOMEONE CALL THE NEWS!!!!!!!!");
	puts("==============================================");
	return (0);
}
