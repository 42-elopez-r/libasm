/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_read.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:08:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/01 19:54:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_read()
** stdio.h: printf(), puts(), remove()
** fnctl.h: open()
** string.h: strlen(), strcmp()
** unistd.h: close(), read(), write()
** stdlib.h: free(), calloc(), realloc()
** errno.h: errno
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define TMP_FILE "dont_mind_me_just_a_temp_file"
#define ERR_TMP_FILE "/dont_mind_me_just_a_temp_file"

/*
** This function runs read() and its libasm equivalent comparing its results.
** Returns true if they match.
*/

static bool	case_read(const char *str, bool successful)
{
	ssize_t ret1, ret2;
	int errno1, errno2;
	int fd;
	char *read_str_orig, *read_str_asm;

	/* Write str to the temporary file */
	if ((fd = open(successful ? TMP_FILE : ERR_TMP_FILE,
			O_WRONLY | O_CREAT | O_TRUNC, 0666)))
	{
		ret1 = ft_write(fd, str, strlen(str));
		close(fd);
	}
	/* ASM read() */
	read_str_asm = calloc(strlen(str) + 1, 1);
	fd = open(successful ? TMP_FILE : ERR_TMP_FILE, O_RDONLY, 0666);
	errno = 0;
	ret1 = ft_read(fd, read_str_asm, strlen(str));
	errno1 = errno;
	if (fd != -1)
		close(fd);

	/* libc read() */
	read_str_orig = calloc(strlen(str) + 1, 1);
	fd = open(successful ? TMP_FILE : ERR_TMP_FILE, O_RDONLY, 0666);
	errno = 0;
	ret2 = read(fd, read_str_orig, strlen(str));
	errno2 = errno;
	if (fd != -1)
		close(fd);

	remove(successful ? TMP_FILE : ERR_TMP_FILE);
	printf("read(): \"%s\" ft_read(): \"%s\"\n", read_str_orig, read_str_asm);
	printf("Orig: %zd ASM: %zd\n", ret2, ret1);
	printf("Errno orig: %d Errno ASM: %d\n", errno2, errno1);
	if (((bool)read_str_asm != (bool)read_str_orig) ||  /* XOR */
			(read_str_asm && read_str_orig &&
			strcmp(read_str_asm, read_str_orig) != 0))
	{
		free(read_str_asm);
		free(read_str_orig);
		return (false);
	}
	free(read_str_asm);
	free(read_str_orig);
	return (ret1 == ret2 && errno1 == errno2);
}

/*
** Test for the read function. Returns true if all cases succeed.
*/

bool	test_read(void)
{
	puts("=== read() tests ===");
	puts("=> Successfull cases");
	if (!case_read("hello world", true))
		return (false);
	if (!case_read("Luna > Celestia", true))
		return (false);
	if (!case_read("I use Arch BTW", true))
		return (false);
	if (!case_read("", true))
		return (false);
	if (!case_read(" ", true))
		return (false);
	puts("=> Unsuccessfull case");
	if (!case_read("hello world", false))
		return (false);
	return (true);
}
