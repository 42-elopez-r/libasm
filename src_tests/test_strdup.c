/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strdup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 20:08:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/03 19:30:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tests.h: This file implements one of its functions.
** libasm.h: ft_strdup()
** stdio.h: printf(), puts()
** stdlib.h: free()
** string.h: strdup(), strcmp()
** errno.h: errno
*/

#include <tests.h>
#include <libasm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/*
** This function runs strdup and its libasm equivalent comparing its results.
** Returns true if they match.
*/

static bool	case_strdup(const char *str)
{
	char	*ret1;
	char	*ret2;
	int		errno1;
	int		errno2;
	int		passed;

	errno = 0;
	ret1 = strdup(str);
	errno1 = errno;
	errno = 0;
	ret2 = ft_strdup(str);
	errno2 = errno;
	printf("strdup(): \"%s\" ft_strdup(): \"%s\"\n", ret1, ret2);
	printf("Errno orig: %d Errno ASM: %d\n", errno1, errno2);
	passed = strcmp(ret1, ret2) == 0;
	free(ret1);
	free(ret2);
	return (passed && errno1 == errno2);
}

/*
** Test for the strdup function. Returns true if all cases succeed.
*/

bool	test_strdup(void)
{
	puts("=== strdup() tests ===");
	if (!case_strdup("hello world"))
		return (false);
	if (!case_strdup("Luna > Celestia"))
		return (false);
	if (!case_strdup("I use Arch BTW"))
		return (false);
	if (!case_strdup(""))
		return (false);
	if (!case_strdup(" "))
		return (false);
	return (true);
}
